with Ada.Integer_TExt_Io; use Ada.Integer_Text_Io;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Long_Integer_TExt_Io; use Ada.Long_Integer_Text_Io;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Ada.Numerics.Generic_Elementary_Functions;

package body numlib is
    package LFEF is new
        Ada.Numerics.Generic_Elementary_Functions (Long_Long_Float);
    use LFEF;

    function DigitsOf(val : Long_Integer) return Digits_Type is
        resultval : Digits_Type ;
        result : NumberList_Values_Type(1..24) := (others=>0);
        valnow : Unsigned_64 := Unsigned_64(val);
    begin
        for dig in reverse result'range 
        loop
            result(dig) := valnow rem 10;
            valnow := valnow / 10;
            if valnow = 0
            then
                resultval := new NumberList_Values_Type(1..24-dig);
                resultval.all := result(dig+1..result'Last);
            end if;
        end loop;
        return resultval;
    end DigitsOf;

    function ValueOf( dig : Digits_Type ; from, to : Integer) return Unsigned_64 is
        result : Unsigned_64 := 0;
    begin
        for idx in from..to
        loop
            result := 10 * result + dig(idx);
        end loop;
        return result;
    end ValueOf;

    function ValueOf( dig : Digits_Type ) return Unsigned_64 is
    begin
        return ValueOf(dig , dig'First, dig'Last);
    end ValueOf;

    function Image( dig : Digits_Type ) return String is
        result : String(Dig'range) := (others=>' ');
        images : String := "0123456789" ;
        ptr : Integer := 0;
    begin
        for d in dig'Range
        loop
            result(d) := images( 1+Integer(dig(d))) ;
            if ptr = 0 and then dig(d) > 0
            then
                ptr := d ;
            end if;
        end loop;
        return result(ptr..result'last) ;
    end Image;

    procedure Sort(vals : NumberList_Type; ascending : boolean := true) is
        temp : Unsigned_64;
    begin
        if ascending
        then
            for j in vals'First..vals'Last-1
            loop
                for i in j..vals'Last-1
                loop
                    if  vals(i) > vals(i+1)
                    then
                        temp := vals(i+1);
                        vals(i+1) := vals(i);
                        vals(i) := temp;
                    end if ;
                end loop;
            end loop;
        else
            for j in vals'First .. vals'Last-1
            loop
                for i in vals'First .. vals'Last-1
                loop
                    if vals(i) < vals(i+1)
                    then
                        temp := vals(i+1);
                        vals(i+1) := vals(i);
                        vals(i) := temp;
                    end if;
                end loop;
            end loop;
        end if ;
    end Sort;

    function Append(old : NumberList_Type; value : Unsigned_64; duplicates : boolean := false ) return NumberList_Type is
        result : NumberList_Type;
    begin
        if old /= null and 
           not duplicates 
        then
            if old(old'Last) = value
            then
                return old;
            end if;
        end if;
        if old = null
        then
            result := new NumberList_Values_Type(1..1);
            result(1) := value;
            return result;
        end if;
        result := new NumberList_Values_Type(1..1+old'Length) ;
        result.all(1..old'Length) := old.all ;
        result(result'Last) := value;
        return result;
    end Append;

    function DivisorsOf(val : Long_Integer) return NumberList_Type is
        result : NumberList_Type;
        unsval : Unsigned_64 := Unsigned_64(val);
        maxdivisor : Unsigned_64 ;
    begin
        maxdivisor := 1 + Unsigned_64(Sqrt(Long_Long_Float(val)));
        for i in 1..maxdivisor
        loop
            if unsval mod i = 0
            then
                result := Append(result,i);
                if unsval/i > i
                then
                    result := Append(result,unsval / i);
                end if;
            end if;
        end loop;
        Sort(result);
        return result;
    end DivisorsOf;

    function FactorsOf(val : Long_Integer) return NumberList_Type is 
        result : NumberList_Type ;
        unsval : Unsigned_64 := Unsigned_64(val);
        maxdivisor : Unsigned_64 ;
    begin
        maxdivisor := 1 + Unsigned_64(Sqrt(Long_Long_Float(val)));
        for i in 2..maxdivisor
        loop
            while unsval mod i = 0
            loop
                result := Append(result,i,True);
                unsval := unsval / i;
            end loop;
        end loop;
        if unsval > 1
        then
            result := Append(result,unsval,True);
        end if;
        Sort(result);
        return result;
    end FactorsOf;

    function ProductOf( numlist : NumberList_Type) return Unsigned_64 is
        result : Unsigned_64 := 1;
    begin
        for nl in numlist'Range
        loop
            result := result * numlist(nl);
        end loop;
        return result;
    end ProductOf;

    function Image(numlist : NumberList_Type) return String is
        result : Unbounded_String :=
                    Null_Unbounded_String;
        needcomma : boolean := false ;
    begin
        for num in numlist'Range
        loop
            if needcomma
            then
                Append(result,",");
            else
                needcomma := true ;
            end if;
            Append(result,Unsigned_64'Image(numlist(num)));
        end loop;
        return To_String(result);
    end Image;

    function IsPrime( num : Long_Integer ) return boolean is
        facs : NumberList_Type := FactorsOf( num ) ;
    begin
        if facs'Length = 1
        then
            return true ;
        end if;
        return false;
    end IsPrime;

    function IsPerfect( num : Long_Integer ) return boolean is
        divs : NumberList_Type := DivisorsOf(num);
        sum : Unsigned_64 := 0;
    begin
        for d in divs'Range
        loop
            sum := sum + divs(d);
        end loop;
        if sum = Unsigned_64(2 * num)
        then
            return true;
        end if;
        return false;
    end IsPerfect;

    function IsNarcissistic( num : Long_Integer ) return boolean is
        digs : Digits_Type := DigitsOf(num);
        p : Integer ;
        sum : Unsigned_64 := 0;
    begin
        for d in digs'Range
        loop
            if digs(d) /= 0
            then
                p := 1 + digs'Length - d ;
                exit;
            end if ;
        end loop;
        for d in digs'Range
        loop
            sum := sum + Unsigned_64( Long_Integer(digs(d)) ** p ) ;
        end loop;
        if sum = Unsigned_64(num)
        then
            return true;
        end if;
        return false;
    end IsNarcissistic;

    function IsKaprekar( num : Long_Integer ) return boolean is
        numsq : Long_Integer := num * num;
        numsqdigs : Digits_Type := DigitsOf(numsq);
        lval,rval : Unsigned_64;
    begin
        for leftlen in 1..numsqdigs'Length-1
        loop
            lval := ValueOf(numsqdigs,numsqdigs'First,numsqdigs'First+leftlen-1) ;
            rval := ValueOf(numsqdigs,numsqdigs'First+leftlen,numsqdigs'Last);
            if rval + lval = Unsigned_64(num)
            then
                return true;
            end if;
        end loop;
        return false;
    end IsKaprekar;

end numlib;