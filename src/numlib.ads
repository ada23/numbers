with Interfaces; use Interfaces;
-- @summary
-- Numbers library
-- @description
-- This package provides a number of basic operations on integers such as
-- digitization - convert a number into an array of digits
-- factorization - compute the factors of a number
-- divisors - compute a list of divisors of a number
package numlib is

    type Digits_Type is private;
    function DigitsOf(val : Long_Integer) return Digits_Type;
    function ValueOf( dig : Digits_Type ) return Unsigned_64;
    function ValueOf( dig : Digits_Type; from,to : Integer) return Unsigned_64;
    
    function Image( dig : Digits_Type ) return String;

    type NumberList_Type is private;

    procedure Sort(vals : NumberList_Type; ascending : boolean := true) ;

    function DivisorsOf(val : Long_Integer) return NumberList_Type;
    function FactorsOf(val : Long_Integer) return NumberList_Type;
    function ProductOf( numlist : NumberList_Type) return Unsigned_64;
    function Image( numlist : NumberList_Type ) return string;

    function IsPrime( num : Long_Integer ) return boolean;
    function IsPerfect( num : Long_Integer ) return boolean;
    function IsNarcissistic( num : Long_Integer ) return boolean;
    function IsKaprekar( num : Long_Integer ) return boolean;

private
    type NumberList_Values_Type is array (integer range <>) of Unsigned_64;
    type NumberList_Type is access all NumberList_Values_Type;
    type Digits_Type is access all NumberList_Values_Type;
end numlib;