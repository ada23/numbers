with Ada.Text_IO;              use Ada.Text_IO;
with Ada.Integer_Text_IO;      use Ada.Integer_Text_IO;
with Ada.Long_Integer_Text_IO; use Ada.Long_Integer_Text_IO;
with Interfaces;               use Interfaces;
with numlib;

procedure Numbers is
   x    : Long_Integer           := 10_560_522;
   d    : numlib.Digits_Type     := numlib.DigitsOf (x);
   v    : Integer                := Integer (numlib.ValueOf (d));
   divs : numlib.NumberList_Type := numlib.DivisorsOf (x);
   y    : Long_Integer           := 8_111;
   facs : numlib.NumberList_Type := numlib.FactorsOf (y);
   val  : Unsigned_64            := numlib.ProductOf (facs);
   procedure primes (val : Long_Integer) is
      facs : numlib.NumberList_Type := numlib.FactorsOf(val);
   begin
      Put (val);
      Put (" IsPrime ");
      Put (Boolean'Image (numlib.IsPrime (val)));
      New_Line;
      Put_Line (numlib.Image (facs));
      numlib.Sort(facs,false);
      Put_Line (numlib.Image (facs));
   end primes;
   procedure perfect (val : Long_Integer) is
   begin
      Put (val);
      Put (" IsPerfect ");
      Put (Boolean'Image (numlib.IsPerfect (val)));
      New_Line;
      Put_Line (numlib.Image (numlib.DivisorsOf (val)));
   end perfect;
   procedure Narcissistic (val : Long_Integer) is
   begin
      Put (val);
      Put (" IsNarcissistic ");
      Put (Boolean'Image (numlib.IsNarcissistic (val)));
      New_Line;
      Put_Line (numlib.Image (numlib.DigitsOf (val)));
   end Narcissistic;
   procedure Kaprekar (val : Long_Integer) is
   begin
      Put (val);
      Put (" IsKaprekar ");
      Put (Boolean'Image (numlib.IsKaprekar (val)));
      New_Line;
      Put_Line (numlib.Image (numlib.DigitsOf (val)));
   end Kaprekar;

begin
   Put (v);
   New_Line;
   Put (numlib.Image (d));
   New_Line;
   Put (numlib.Image (divs));
   New_Line;
   Put (numlib.Image (facs));
   New_Line;
   Put (Integer (val));
   New_Line;

   primes (8_111);
   primes (18_111);
   primes (34_866);

   perfect (6);
   perfect (124);
   perfect (496);
   perfect (8_589_869_056);
   perfect (137_438_691_328);

   Narcissistic (371);
   Narcissistic (24_678_050);
   Narcissistic (24_678_050_371);

   Kaprekar(703);
   Kaprekar(5292);
   Kaprekar(52_925_292);
end Numbers;
